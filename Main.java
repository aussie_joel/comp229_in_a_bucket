import java.util.*;

public class Main {

	public static void main(String[] args) {
		String[] food = {"tin food", "fish"};
		Cat c = new Cat(true, food, 4, "Black");
				
		//Notice that I am accessing something from animal through a Cat object
		System.out.println("Cat is Vegetarian?" + c.getVegitarian());
		System.out.println("Cat eats " + Arrays.toString(c.getEats()));
		System.out.println("Cat has " + c.getnumOfLegs() + " legs.");
		System.out.println("Cat color is " + c.getColour());
		
		//Calling the function we over wrote
		c.overWriteMe();
	}

}

class Cat extends Animal { //by having extends it means we can access Animals information/functions which are not private
	private String colour;
	public Cat(boolean veg, String[] food, int numLegs) {
		//super allows me to call the constructor of Animal
		super(veg, food, numLegs);
		this.colour = "White";
		//Notice I can access gender as it is a  public variable (no private in front of the variable in animal class)
		this.gender = "male";
		/*
		 * Note: this piece of code below will cause an error as it is private in the animal class
		 		* if it was not commented out
		 * vegitarian = false;
		 */
	}
	
	public Cat(boolean veg, String[] food, int numLegs, String col) {
		//super allows me to call the constructor of Animal
		super(veg, food, numLegs);
		this.colour = col;
	}
	
	/**
	 * Notice we Changed the function to print out string from the empty function contained in Animal
	 */
	@Override
	public void overWriteMe() {
		System.out.println("Overwiten bitches!!!x");
	}
	
	public String getColour() {
		return colour;
	}
	
	public void setColour(String colour) {
		this.colour = colour;
	}
}

class Animal {
	private boolean vegitarian;
	private String[] eats;
	private int numOfLegs;
	String gender;
	
	/** constructors for Animal **/
	/**
	 * Empty constructor
	 */
	public Animal() {
	}
	/**
	 * Non-empty constructor
	 * @param vegitarian Do they eat meat?
	 * @param eats List of food they eat
	 * @param numOfLegs How many legs they have
	 */
	public Animal(boolean vegitarian, String[] eats, int numOfLegs) {
		this.vegitarian =  vegitarian;
		this.eats = eats;
		this.numOfLegs = numOfLegs;
	}
	
	/**
	 * Non-empty constructor
	 * @param vegitarian Do they eat meat?
	 * @param eats List of food they eat
	 * @param numOfLegs How many legs they have
	 * @param gender are you female, male or some other sort of bs xD
	 */
	public Animal(boolean vegitarian, String[] eats, int numOfLegs, String gender) {
		this.vegitarian =  vegitarian;
		this.eats = eats;
		this.numOfLegs = numOfLegs;
		this.gender = gender;
	}
	
	/** Functions we are going  to overwrite **/
	public void  overWriteMe() {
		
	}
	
	/******* Setters and getters *******/
	
	/** Stuff to do with numOfLegs **/
	public void setnumOfLegs(int num) {
		numOfLegs = num;
	}
	
	public int getnumOfLegs() {
		return numOfLegs;
	}
	
	
	/** Stuff to do with vegitarian **/
	public void setVegitarian(boolean b) {
		vegitarian = b;
	}
	
	public boolean getVegitarian() {
		return vegitarian;
	}
	
	
	/** Stuff to do with eats **/
	public void setEats(String[] e) {
		eats = e;
	}
	
	public String[] getEats() {
		return eats;
	}
}
